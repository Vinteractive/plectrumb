<?php
/*
 * Loop Generator: WonderLoops
 * Loop Name: Home Services
 * Loop File: 55.php
 */
wp_enqueue_style( 'js_composer_front' );
wp_enqueue_script( 'wpb_composer_front_js' );
wp_enqueue_style('js_composer_custom_css');
wp_enqueue_script('jquery-ui-accordion');
wp_enqueue_script( 'jquery-ui-tabs' );
wp_enqueue_script('jquery_ui_tabs_rotate');
require_once (WONDERLOOPS_INCLUDES.'/call_functions.php');
?>

<?php
if(!is_singular() || $a_custom_loop){
}
$igogrid=1;
if($a_custom_loop){
if ($r->have_posts() ) {
while ( $r->have_posts() ) { 
$r->the_post();
global $post;
$clear=true;
if($colcount!=1)://gridd
if($igogrid==1){
$igogrid++;
remove_filter( "post_class", "ultimatum_entry_post_class_last" );
$clear=false;
} elseif($igogrid==$colcount){
add_filter( "post_class", "ultimatum_entry_post_class_last" );
$clear=true;
$igogrid=1;
} else{
$igogrid++;
remove_filter( "post_class", "ultimatum_entry_post_class_last" );
$clear=false;
}
endif;//gridd
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="wpb_row vc_row-fluid">
 <div class="vc_span6">
<h1>Testing ACF and Wonder Loops</h1>
 <?php the_field('testimonial_quote'); ?>
 </div>
 <div class="vc_span6">
  
 </div>
</div>
<div class="vc_row wpb_row vc_row-fluid">
	<div class="vc_col-sm-6 wpb_column vc_column_container">
		<div class="wpb_wrapper">
			<a href="<?php the_permalink();?>"><?php 
$attr = array("class"	=> "attachment-full ");the_post_thumbnail("full",$attr);
?>
</a><?php the_field("testimonial_quote"); ?><?php the_field("client_name"); ?><?php the_field("client_location"); ?>
		</div> 
	</div> 

	<div class="vc_col-sm-6 wpb_column vc_column_container">
		<div class="wpb_wrapper">
			<?php 
$attr = array("class"	=> "attachment-full ");the_post_thumbnail("full",$attr);
?>
<?php the_field("testimonial_quote"); ?><?php the_field("client_name"); ?><?php the_field("client_location"); ?>
		</div> 
	</div> 
</div>
</article>
<?php
if($clear) echo '<div class="clearfix"></div>';
} // end while
} else {
do_action( "ultimatum_loop_else" );
} // end if
?>
<?php
} else {
if ( have_posts() ) {
while ( have_posts() ) { 
the_post();
global $post;
$clear=true;
if($colcount!=1 && !is_singular() )://gridd
if($igogrid==1){
$igogrid++;
remove_filter( "post_class", "ultimatum_entry_post_class_last" );
$clear=false;
} elseif($igogrid==$colcount){
add_filter( "post_class", "ultimatum_entry_post_class_last" );
$clear=true;
$igogrid=1;
} else{
$igogrid++;
remove_filter( "post_class", "ultimatum_entry_post_class_last" );
$clear=false;
}
endif;//gridd
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="wpb_row vc_row-fluid">
 <div class="vc_span6">
<h1>Testing ACF and Wonder Loops</h1>
 <?php the_field('testimonial_quote'); ?>
 </div>
 <div class="vc_span6">
  
 </div>
</div>
<div class="vc_row wpb_row vc_row-fluid">
	<div class="vc_col-sm-6 wpb_column vc_column_container">
		<div class="wpb_wrapper">
			<a href="<?php the_permalink();?>"><?php 
$attr = array("class"	=> "attachment-full ");the_post_thumbnail("full",$attr);
?>
</a><?php the_field("testimonial_quote"); ?><?php the_field("client_name"); ?><?php the_field("client_location"); ?>
		</div> 
	</div> 

	<div class="vc_col-sm-6 wpb_column vc_column_container">
		<div class="wpb_wrapper">
			<?php 
$attr = array("class"	=> "attachment-full ");the_post_thumbnail("full",$attr);
?>
<?php the_field("testimonial_quote"); ?><?php the_field("client_name"); ?><?php the_field("client_location"); ?>
		</div> 
	</div> 
</div>
</article>
<?php
if($clear) echo '<div class="clearfix"></div>';
} // end while
do_action( "ultimatum_after_endwhile",$instance );
 } else {
do_action( "ultimatum_loop_else" );
} // end if
?>
<?php
}
?>

