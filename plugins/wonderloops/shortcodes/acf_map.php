<?php
/*
 * ACF Map
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_acf_map extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'field' => '',
			'height' => '400',
			), $atts));
			$content ="\n";
			$content .= '<?php'."\n";
			$content .='$location = get_field("'.$field.'");'."\n";
			$content .='if( !empty($location) ){'."\n";
			$content .= '?>'."\n";
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content .= '<?php $atts = array("height"=>"'.$height.'","latitude"=>$location["lat"],"longitude"=>$location["lng"]); echo wflb_ggogle_maps($atts);?>';
			$content .= wfl_tag_close($wrapper);
			$content .= '<?php'."\n";
			$content .= '}'."\n";
			$content .= '?>'."\n";
			return $content;
		}
	}
	

}



