<?php
/*
 * Content
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_content extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'read_more' => '',
			'read_more_text' => __('Read More', 'wonder_loops'),
			'read_more_class' => '',
			), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content .= "<?php the_content();?>";
			if($read_more=='yes'){
				$content .= '<?php if(!is_singular()){ ?>'."\n";
				$content .= '<a class="'.$read_more_class.'" href="<?php the_permalink();?>" title="<?php the_title();?>">'.$read_more_text.'</a>'."\n";
				$content .= '<?php }?>'."\n";
			}
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}



