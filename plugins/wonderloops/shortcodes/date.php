<?php
if(class_exists('WPBakeryShortCode'))
{
	class WPBakeryShortCode_wonderloops_date extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => 'none',
			'wrapper_class' => '',
			'pre_text'=> '',
			'pre_text_tag'=>'none',
			'date_format'=> 'l, F j, Y',
			'seperator'=>'',
			), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content .= wfl_pre_text($pre_text,$pre_text_tag);
			if(strlen($seperator)!=0){
				$dateray= explode($seperator, $date_format);
				$i=1;
				foreach($dateray as $data){
					$content .= '<span class="date_span_'.$i.'"><?php echo get_the_date("'.trim($data).'");?></span>'."\n";
					$i++;
				}
			} else {
				$content .= '<?php echo get_the_date("'.$date_format.'");?>'."\n";
			}
			$content .= wfl_tag_close($wrapper);
			return $content;
		}


	}
}