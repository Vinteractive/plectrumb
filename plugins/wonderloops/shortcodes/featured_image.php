<?php
/*
 * Featured Image
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_fimage extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
				'wrapper' => '',
				'wrapper_class' => '',
				'item_class'=>'',
				'on_click'=>'none',
				), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			if($on_click=="zoom"){
				$content .= '<?php'.
				' $img_id=get_post_thumbnail_id();'.
				' $src = wp_get_attachment_image_src( $img_id,"full");'.
				' ?>';
				
				$content .= '<a href="<?php echo $src[0]; ?>" class="prettyphoto" alt="<?php the_title();?>" title="<?php the_title();?>">';
			} elseif ($on_click=='to_post'){
				$content .= '<a href="<?php the_permalink();?>">';
			}
			$content .= "<?php "."\n";
			$content .= '$attr = array("class"	=> "attachment-full '.$item_class.'");';
			$content .= 'the_post_thumbnail("full",$attr);'."\n";
			$content .= "?>"."\n";
			if($on_click!="none"){
				$content .= '</a>';
			}
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}

