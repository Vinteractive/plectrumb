<?php
/*
 * Content
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_comments extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'pre_text' => '',
			'linked' => '',
			), $atts));
			$content .= '<?php ultimatum_get_comments_template($instance);?>'."\n";
			return $content;
		}
	}
	
	class WPBakeryShortCode_wonderloops_comments_count extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'pre_text' => '',
			), $atts));
			
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			if(strlen($pre_text)!=0){
				$content .= $pre_text.' ';
			}
			$content .= '<?php echo get_comments_number(); ?>'."\n";
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}

}



