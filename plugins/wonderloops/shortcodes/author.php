<?php
/*
 * Content
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_author extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'pre_text' => '',
			'linked' => '',
			), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			if(strlen($pre_text)!=0){
				$content .= $pre_text.' ';
			}
			if($linked =='yes'){
				$content .= "<?php the_author_posts_link();?>";
			} else {
				$content .= "<?php the_author();?>";
			}
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}



