<?php
/*
 * ACF Common
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_acf_checkbox extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => 'none',
			'wrapper_class' => '',
			'pre_text' =>'',
			'items_wrapper' => '',
			'items_wrapper_class' => '',
			'itemtag' => 'span',
			'item_class' => '',
			'seperator' => '',
			'field' => '',
			), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content.= "\n";
			$content .= '<?php $field = get_field_object("'.$field.'");'."\n";
			$content .= 'if($field){'."\n";
			$content .= '?>'."\n";
			if(strlen($pre_text)!=0){
				$content .= $pre_text.' ';
			}
			$content .= wfl_tag_open($items_wrapper,$items_wrapper_class);
			$content .= '<?php $values = get_field("'.$field.'");'."\n";
			$content .= 'if(is_array($values)){'."\n";
			$content .= 'foreach($values as $value){'."\n";
			$content .= '$label = $field["choices"][ $value ];'."\n";
			$content .= '?><'.$itemtag.' class="<?php echo $value;?>"><?php echo $label;?></'.$itemtag.'><?php'."\n";
			$content .= '}'."\n";
			$content .= '} ?>'."\n";
			$content .= wfl_tag_close($items_wrapper);
			$content .= '<?php } ?>'."\n";
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}



