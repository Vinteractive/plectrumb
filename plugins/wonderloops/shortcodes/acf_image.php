<?php
/*
 * ACF Image
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_acf_image extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'field' => '',
			'caption' =>'',
			'zoom' =>'',
			), $atts));
			$content ="\n";
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content .= '<?php'."\n";
			$content .= '$image = get_field("'.$field.'");'."\n";
			$content .= 'if( !empty($image) ){'."\n";
			$content .= '?>'."\n";
			if( $caption =='yes'){
			$content .= '<div class="wp-caption">'."\n";
			}
			if( $zoom =='yes'){
			$content .= '<a href="<?php echo $image["url"]; ?>" title="<?php echo $image["title"]; ?>" class="prettyphoto">'."\n";
			}
			$content .= '<img src="<?php echo $image["url"]; ?>" alt="<?php echo $image["alt"];; ?>" />'."\n";
			if( $zoom =='yes'){
				$content .= '</a>'."\n";
			}
			if( $caption =='yes' ){
			$content .= '<p class="wp-caption-text"><?php echo $image["caption"]; ?></p>'."\n";
			$content .= '</div>'."\n";
			}
			$content .= '<?php } ?>'."\n";
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}



