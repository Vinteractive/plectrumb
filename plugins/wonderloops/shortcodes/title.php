<?php
/*
 * Title
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_title extends WPBakeryShortCode {
		
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
				'wrapper' => '',
				'wrapper_class' => '',
				'itemtag'=>'h1',
				'itemclass'=>'',
				'link_to_post' => ''
				), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content .= wfl_tag_open($itemtag,$itemclass);
			if($link_to_post=='yes'){
				$content .= '<a href="<?php the_permalink();?>" title="<?php the_title();?>">'."\n";
			}
			$content .= "<?php the_title();?>"."\n";
			if($link_to_post=='yes'){
				$content .= '</a>'."\n";
			}
			$content .= wfl_tag_close($itemtag);
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}



