<?php
/*
 * Excerpt
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_excerpt extends WPBakeryShortCode {
		function content($atts, $content = null) {
				extract(shortcode_atts(array(
				'wrapper' => '',
				'wrapper_class' => '',
				'read_more' => '',
				'read_more_text' => __('Read More', 'wonder_loops'),
				'read_more_class' => '',
				), $atts));
				$content ='';
				$content .= wfl_tag_open($wrapper,$wrapper_class);
				$content .= "<?php the_excerpt();?>";
				if($read_more=='yes'){
					$content .= '<a class="'.$read_more_class.'" href="<?php the_permalink();?>" title="<?php the_title();?>">'.$read_more_text.'</a>'."\n";
				}
				$content .= wfl_tag_close($wrapper);
				return $content;
			}
	}
	

}