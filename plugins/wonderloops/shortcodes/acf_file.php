<?php
/*
 * ACF File
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_acf_file extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'field' => '',
			'text' =>'',
			'member_only' =>'',
			), $atts));
			$content ="\n";
			if($member_only=='yes'){
			$content .= '<?php if ( is_user_logged_in() ) { ?>'."\n";
			}
			
			$content .= '<?php if( get_field("'.$field.'") ){ ?>'."\n";
			$content .= '<?php $file = get_field("'.$field.'") ?>'."\n";
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content .= '<a href="<?php echo $file["url"]; ?>" class="btn">'.$text.'</a>'."\n";
			$content .= wfl_tag_close($wrapper);
			$content .= '<?php } ?>'."\n";
			if($member_only=='yes'){
			$content .= '<?php } ?>'."\n";
			}
			
			
			return $content;
		}
	}
	

}



