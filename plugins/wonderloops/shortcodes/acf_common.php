<?php
/*
 * ACF Common
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_acf extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'pre_text' => '',
			'field' => '',
			), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			if(strlen($pre_text)!=0){
				$content .= $pre_text.' ';
			}
			$content .= '<?php the_field("'.$field.'"); ?>';
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}



