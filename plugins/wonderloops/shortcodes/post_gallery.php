<?php

if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_ult_post_gallery extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => '',
			'wrapper_class' => '',
			'pre_text' => '',
			'zoom' => '',
			), $atts));
			$content .= '<?php wflb_post_gallery($instance,"'.$zoom.'");?>'."\n";
			return $content;
		}
	}
}
