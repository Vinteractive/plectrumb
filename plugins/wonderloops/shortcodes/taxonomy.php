<?php
/*
 * Content
*/
if(class_exists('WPBakeryShortCode'))
{

	class WPBakeryShortCode_wonderloops_tax extends WPBakeryShortCode {
		function content($atts, $content = null) {
			extract(shortcode_atts(array(
			'wrapper' => 'none',
			'wrapper_class' => '',
			'pre_text' =>'',
			'items_wrapper' => '',
			'items_wrapper_class' => '',
			'itemtag' => 'none',
			'item_class' => '',
			'seperator' => '',
			'taxonomy'=>'',
			), $atts));
			$content ='';
			$content .= wfl_tag_open($wrapper,$wrapper_class);
			$content .="\n";
			$content .= '<?php '."\n";
			$content .= '$'.$taxonomy.'_terms = wp_get_object_terms($post->ID, "'.$taxonomy.'");'."\n";
			$content .= 'if(!empty($'.$taxonomy.'_terms) && !is_wp_error( $'.$taxonomy.'_terms )){'."\n";
			$content .=' ?>'."\n";
			if(strlen($pre_text)!=0){
				$content .= $pre_text.' ';
			}
			$content .= wfl_tag_open($items_wrapper,$items_wrapper_class);
			$content .= '<?php '."\n";
			$content .= '$tc=0;'."\n";
			$content .= 'foreach($'.$taxonomy.'_terms as $term){'."\n";
			$content .= '$tc++;'."\n";
			$content .= '?>'."\n";
			$content .= wfl_tag_open($itemtag,$item_class);
			$content .= '<a href="<?php echo get_term_link($term->slug, "'.$taxonomy.'")'.'?>"><?php echo $term->name;?></a>'."\n";
			$content .= wfl_tag_close($itemtag);
			if(strlen($seperator)!=0){
				$content .= '<?php '."\n";
				$content .= 'if($tc!=count($'.$taxonomy.'_terms)){ echo "'.$seperator.'";}'."\n";
				$content .=' ?>'."\n";
			}
			$content .= '<?php '."\n";
			$content .= '}'."\n";
			$content .=' ?>'."\n";
			$content .= wfl_tag_close($items_wrapper);
			$content .= '<?php '."\n";
			$content .= '} '."\n";
			$content .=' ?>'."\n";
			
			$content .= wfl_tag_close($wrapper);
			return $content;
		}
	}
	

}



