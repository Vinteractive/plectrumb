<?php
/*
Plugin Name: Wonder Loops
Plugin URI: http://www.wonderfoundry.com
Description: Drag and drop loop builder for WordPress using the power of ACF.
Version: 1.0.0b
Author: Onur Demir
Author URI: http://onurdemir.com
ULT ID: 19
*/

$current_default= get_option( 'stylesheet' );
$theme = wp_get_theme( $current_default );
if(strtolower($theme->get('Template')) =='ultimatum' || strtolower($theme->get('Name')) =='ultimatum'){
		
$upload_dir = wp_upload_dir();
$uploaddir = $upload_dir["basedir"];
$uploadurl = $upload_dir["baseurl"];
define('WONDERLOOPS_CACHE_DIR', $uploaddir.'/wonderloops');
if(!is_dir(WONDERLOOPS_CACHE_DIR)) mkdir(WONDERLOOPS_CACHE_DIR);


define( 'WONDERLOOPS', plugin_dir_path( __FILE__  ) );
define( 'WONDERLOOPS_INCLUDES', WONDERLOOPS.'includes' );
define( 'WONDERLOOPS_SHORTCODES', WONDERLOOPS.'shortcodes' );
define( 'WONDERLOOPS_URL', plugin_dir_url( __FILE__ ) );
define( 'WONDERLOOPS_IMAGES', WONDERLOOPS_URL.'assets/images/');
define( 'WONDERLOOPS_CSS', WONDERLOOPS_URL.'assets/css/');
define( 'WONDERLOOPS_JS', WONDERLOOPS_URL.'assets/js/');


// Get the very Customized ACF plugin
require_once('advanced-custom-fields/acf.php' );



// Get the classes
require_once('includes/wonderloops.admin.php' );
require_once('includes/wonderloops.front.php' );
$pt_array = get_option('wpb_js_content_types');
if (!is_array($pt_array) || empty($pt_array)) {
	$pt_array = array('wonderloops', 'page');
	update_option('wpb_js_content_types', $pt_array);
} elseif (!in_array('wonderloops', $pt_array)) {
	$pt_array[] = 'wonderloops';
	update_option('wpb_js_content_types', $pt_array);
}

class WonderLoops {
	
	function __construct() {
		$this->addPostType();
		if( is_admin() ) {
			// Get the helper functions
			require_once('includes/helpers.php' );
			add_action( 'admin_init', array($this,'hide_editor') );
			new WonderLoopsAdmin();
		} 
		
	}
	
	function hide_editor() {
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-ptype',true);
		if(wonderloops_is_post_type('wonderloops') && strlen($cura)==0){
			remove_post_type_support('wonderloops', 'editor');
		}
	}
	
	function addPostType(){
		$labels = array(
				'name' => __( 'Loops', 'wonderloops' ),
				'singular_name' => __( 'Loops', 'wonderloops' ),
				'add_new' => __( 'Add New' , 'wonderloops' ),
				'add_new_item' => __( 'Add New Loop' , 'wonderloops' ),
				'edit_item' =>  __( 'Edit Loop' , 'wonderloops' ),
				'new_item' => __( 'New Loop' , 'wonderloops' ),
				'view_item' => __('View Loop', 'wonderloops'),
				'search_items' => __('Search Loops', 'wonderloops'),
				'not_found' =>  __('No Loops found', 'wonderloops'),
				'not_found_in_trash' => __('No Loops found in Trash', 'wonderloops'),
		);
	
		register_post_type('wonderloops', array(
		'labels' => $labels,
		'public' => false,
		'show_ui' => true,
		'_builtin' =>  false,
		'capability_type' => 'page',
		'hierarchical' => true,
		'rewrite' => false,
		'query_var' => "wonderloops",
		'supports' => array(
		'title','editor'
		),
		'show_in_menu'	=> true,
		));
	}
	
}
if(defined('WPB_VC_VERSION')){
new WonderLoops();
} else {
	add_action( 'admin_notices', 'wfl_admin_notice_for_vc_activation');
}
}
function wfl_admin_notice_for_vc_activation()
{
	echo '<div class="updated"><p>The <strong>Wonder Loops</strong> plugin requires <strong>Visual Composer</strong> Plugin installed and activated.</p></div>';
}