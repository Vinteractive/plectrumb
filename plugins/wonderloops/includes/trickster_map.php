<?php
/*
 * This map is used to trick the VC so that Shortcodes are parsed
 */

/*
 * WP Post Defaults
 */
 
// The Date
vc_map(array(
"name" => __('Post Date', 'wonder_loops'),
"base" => 'wonderloops_date',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Date pof the Post', 'wonder_loops'),
));

// The Title
vc_map(array(
"name" => __('Post Title', 'wonder_loops'),
"base" => 'wonderloops_title',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Title of Current Post', 'wonder_loops'),
));

// Content
vc_map(array(
"name" => __('Post Content', 'wonder_loops'),
"base" => 'wonderloops_content',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Title of Current Post', 'wonder_loops'),
));

// Featured Image
vc_map(array(
"name" => __('Feautred Image', 'wonder_loops'),
"base" => 'wonderloops_fimage',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Featured Image', 'wonder_loops'),
));

// Excerpt
vc_map(array(
"name" => __('Post Excerpt', 'wonder_loops'),
"base" => 'wonderloops_excerpt',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Excerpt of Current Post', 'wonder_loops'),
));

// Taxonomy
vc_map(array(
"name" => 'Taxonomy',
"base" => 'wonderloops_tax',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
));
// Author
vc_map(array(
"name" => __('Author', 'wonder_loops'),
"base" => 'wonderloops_author',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Author of Current Post', 'wonder_loops'),
));
//comments
vc_map(array(
"name" => __('Post Comments Count', 'wonder_loops'),
"base" => 'wonderloops_comments_count',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Shows Count of Comments', 'wonder_loops'),
));
vc_map(array(
"name" => __('Post Comments', 'wonder_loops'),
"base" => 'wonderloops_comments',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Shows Comments & Comment Form', 'wonder_loops'),
));
vc_map(array(
"name" => __('ACF COMMON', 'wonder_loops'),
"base" => 'wonderloops_acf',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
"description" => __('Shows Comments & Comment Form', 'wonder_loops'),
));
vc_map(array(
"name" => __('ACF MAP', 'wonder_loops'),
"base" => 'wonderloops_acf_map',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
));
vc_map(array(
"name" => __('ACF IMAGE', 'wonder_loops'),
"base" => 'wonderloops_acf_image',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
));
vc_map(array(
"name" => __('ACF FILE', 'wonder_loops'),
"base" => 'wonderloops_acf_file',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
));
vc_map(array(
"name" => __('ACF SELECT', 'wonder_loops'),
"base" => 'wonderloops_acf_select',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
));
vc_map(array(
"name" => __('ACF CKBX', 'wonder_loops'),
"base" => 'wonderloops_acf_checkbox',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
));
vc_map(array(
"name" => __('Post Gallery', 'wonder_loops'),
"base" => 'wonderloops_ult_post_gallery',
"icon" => "icon-wf-wp",
"category" => 'WP Loop Core',
"weight" => -50,
));
