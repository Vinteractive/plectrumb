<?php

function wf_hidden_settings_field($settings, $value) {
	$dependency = vc_generate_dependencies_attributes($settings);
	return '<input name="'.$settings['param_name'].'" class="wpb_vc_param_value wpb-textinput '.$settings['param_name'].' '.$settings['type'].'_field" type="hidden" value="'.$value.'" ' . $dependency . '/>';
}
add_shortcode_param('hidden', 'wf_hidden_settings_field');

function get_acf_fields($post_id=null){
	$ultlbelements = array();
	if(!$post_id){
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if(isset($_POST['action']) && $_POST['action']=='wpb_show_edit_form' && isset($_POST['post_id'])){
		$post_id = $_POST['post_id'];
	}
	}
	if(isset($post_id)){
		$cura= get_post_meta($post_id,'wonderloops-ptype',true);
		// Ultimatum Gallery
		$checks = get_option('ultimatum_postgals');
		if($checks[$cura]==1){
			vc_map(array(
			"name" => __('Ultimatum Post Gallery', 'wonder_loops'),
			"base" => 'wonderloops_ult_post_gallery',
			"icon" => "icon-wf-wp",
			"category" => 'Ultimatum',
			"weight" => -50,
			"description" => __('Flex Slider of post gallery', 'wonder_loops'),
			"params" => array(array(
				'type' => 'checkbox',
				'heading' => __( 'Zoom on Click', 'wonder_loops' ),
				'param_name' => 'zoom',
				'description' => __( 'If selected,on click prettyphoto will zoom', 'wonder_loops' ),
				'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
				))
			));
		}
		
		//echo $cura;
		vc_map(array(
			"name" => __('Post Comments Count', 'wonder_loops'),
			"base" => 'wonderloops_comments_count',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Shows Count of Comments', 'wonder_loops'),
			"params" =>	array(
					wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
					array(
							"type" => "textfield",
							"heading" => __("Pre Text", "wonder_loops"),
							"param_name" => "pre_text",
							"description" => __('Any text you want to place before value. Eg: Published On:', 'wonder_loops'),
					),
					array(
							"type" => "textfield",
							"heading" => __("Wrapper class name", "wonder_loops"),
							"param_name" => "wrapper_class",
					),
			)
		));
		vc_map(array(
			"name" => __('Post Comments', 'wonder_loops'),
			"base" => 'wonderloops_comments',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Shows Comments & Comment Form', 'wonder_loops'),
			));
		vc_map(array(
			"name" => __('Post Date', 'wonder_loops'),
			"base" => 'wonderloops_date',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Date pof the Post', 'wonder_loops'),
			"params" =>	array(
							wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
							array(
									"type" => "textfield",
									"heading" => __("Pre Text", "wonder_loops"),
									"param_name" => "pre_text",
									"description" => __('Any text you want to place before value. Eg: Published On:', 'wonder_loops'),
							),
							wfl_select_tags('pre_text_tag',__('Pre text tag','wonder_loops'),'none',array('none','div','span','p')),
							array(
									"type" => "textfield",
									"heading" => __("Date Format", "wonder_loops"),
									"param_name" => "date_format",
									"description" => __('Type in format you want to show eg l, F j, Y you can see variables on <a href="http://codex.wordpress.org/Formatting_Date_and_Time" target="_blank">WP Codex</a>', 'wonder_loops'),
							),
							array(
									"type" => "textfield",
									"heading" => __("Span Seperator", "wonder_loops"),
									"param_name" => "seperator",
									"description" => __("To wrap each value with spans let us know your seperator eg , is seperator for the above example format.Leave blank if no spans needed.", "wonder_loops"),
							),
							array(
									"type" => "textfield",
									"heading" => __("Wrapper class name", "wonder_loops"),
									"param_name" => "wrapper_class",
							),
							
						)
			));
		if(post_type_supports( $cura, 'title' ) ){
			vc_map(array(
			"name" => __('Post Title', 'wonder_loops'),
			"base" => 'wonderloops_title',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Title of Current Post', 'wonder_loops'),
			"params" =>	array(
							wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
							wfl_select_tags('itemtag',__('Item Tag','wonder_loops'),'h1',array('none','h1','h2','h3','h4','h5','h6','p','div','span')),
							wfl_link_to_post(),
							array(
									"type" => "textfield",
									"heading" => __("Wrapper class name", "wonder_loops"),
									"param_name" => "wrapper_class",
							),
							array(
									"type" => "textfield",
									"heading" => __("Item class name", "wonder_loops"),
									"param_name" => "item_class",
							),
						)
			));
		}
		if(post_type_supports( $cura, 'editor' ) ){
			vc_map(array(
			"name" => __('Post Content', 'wonder_loops'),
			"base" => 'wonderloops_content',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Content of Current Post', 'wonder_loops'),
			"params" =>	array(
							wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
							array(
									'type' => 'checkbox',
									'heading' => __( 'Show Read More?', 'wonder_loops' ),
									'param_name' => 'read_more',
									'description' => __( 'If selected, Read More will be shown.', 'wonder_loops' ),
									'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
							),
							array(
									"type" => "textfield",
									"heading" => __("Read More Text", "wonder_loops"),
									"param_name" => "read_more_text",
							),
							array(
									"type" => "textfield",
									"heading" => __("Read More Class", "wonder_loops"),
									"param_name" => "read_more_class",
							),
							array(
									"type" => "textfield",
									"heading" => __("Wrapper class name", "wonder_loops"),
									"param_name" => "wrapper_class",
							),
						)
			));
		}
		if(post_type_supports( $cura, 'thumbnail' ) ){
			vc_map(array(
			"name" => __('Feautred Image', 'wonder_loops'),
			"base" => 'wonderloops_fimage',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Featured Image', 'wonder_loops'),
			"params" =>	array(
							wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
							array(
									"type" => "dropdown",
									"heading" => __("On Click", "wonder_loops"),
									"param_name" => "on_click",
									'value' => array('none','to_post','zoom')
							),
							array(
									"type" => "textfield",
									"heading" => __("Wrapper class name", "wonder_loops"),
									"param_name" => "wrapper_class",
							),
							array(
									"type" => "textfield",
									"heading" => __("Item class name", "wonder_loops"),
									"param_name" => "item_class",
							),
						)
			));
		}
		if(post_type_supports( $cura, 'author' ) ){
			vc_map(array(
			"name" => __('Author', 'wonder_loops'),
			"base" => 'wonderloops_author',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Author of Current Post', 'wonder_loops'),
			"params" =>	array(
							wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
							array(
									"type" => "textfield",
									"heading" => __("Pre Text", "wonder_loops"),
									"param_name" => "pre_text",
							),
							array(
									'type' => 'checkbox',
									'heading' => __( 'Link to Author?', 'wonder_loops' ),
									'param_name' => 'linked',
									'description' => __( 'If selected, Author name will be linked to authors posts page.', 'wonder_loops' ),
									'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
							),
							array(
									"type" => "textfield",
									"heading" => __("Wrapper class name", "wonder_loops"),
									"param_name" => "wrapper_class",
							),
						)
			));
		}
		if(post_type_supports( $cura, 'excerpt' ) ){
			vc_map(array(
			"name" => __('Post Excerpt', 'wonder_loops'),
			"base" => 'wonderloops_excerpt',
			"icon" => "icon-wf-wp",
			"category" => 'WP Loop Core',
			"weight" => -50,
			"description" => __('Excerpt of Current Post', 'wonder_loops'),
			"params" =>	array(
							wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
							array(
									'type' => 'checkbox',
									'heading' => __( 'Show Read More?', 'wonder_loops' ),
									'param_name' => 'read_more',
									'description' => __( 'If selected, Read More will be shown.', 'wonder_loops' ),
									'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
							),
							array(
									"type" => "textfield",
									"heading" => __("Read More Text", "wonder_loops"),
									"param_name" => "read_more_text",
							),
							array(
									"type" => "textfield",
									"heading" => __("Read More Class", "wonder_loops"),
									"param_name" => "read_more_class",
							),
							array(
									"type" => "textfield",
									"heading" => __("Wrapper class name", "wonder_loops"),
									"param_name" => "wrapper_class",
							),
						)
			));
		}
		
		// Taxonomies
		
		$taxonomy_objects = get_object_taxonomies( $cura, 'objects' );
		foreach ($taxonomy_objects as $taxonomy => $taxprops){
			vc_map(array(
				"name" => $taxprops->label,
				"base" => 'wonderloops_tax_wflb_'.$taxonomy.'_wflb',
				"icon" => "icon-wf-wp",
				"category" => 'WP Loop Core',
				"weight" => -50,
				"params" =>	array(
					array(
						"type" => "hidden",
						"param_name" => "taxonomy",
						'value' => $taxonomy,
					),
					wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
					wfl_select_tags('items_wrapper',__('Wrap Items in','wonder_loops'),'h1',array('none','p','div','span','ul','ol')),
					wfl_select_tags('itemtag',__('Item Tag','wonder_loops'),'h1',array('none','p','div','span','li')),
					array(
							"type" => "textfield",
							"heading" => __("Pre Text", "wonder_loops"),
							"param_name" => "pre_text",
					),
					array(
						"type" => "textfield",
						"heading" => __("Seperator", "wonder_loops"),
						"param_name" => "seperator",
					),
					array(
						"type" => "textfield",
						"heading" => __("Wrapper class name", "wonder_loops"),
						"param_name" => "wrapper_class",
					),
					array(
							"type" => "textfield",
							"heading" => __("Items Wrapper class name", "wonder_loops"),
							"param_name" => "item__wrapper_class",
					),
					array(
						"type" => "textfield",
						"heading" => __("Item class name", "wonder_loops"),
						"param_name" => "item_class",
					),
				)
			));
		}
		// get field groups
		$filter = array(
				'post_type'	=> $cura
		);
		$metabox_ids = array();
		$metabox_ids = apply_filters( 'acf/location/match_field_groups', $metabox_ids, $filter );
		$acfs = apply_filters('acf/get_field_groups', array());
		if( $acfs ){
			foreach( $acfs as $acf ){
				$acf['options'] = apply_filters('acf/field_group/get_options', array(), $acf['id']);
				$show = in_array( $acf['id'], $metabox_ids ) ? 1 : 0;
				if($show===1){
					$fields = apply_filters('acf/field_group/get_fields', array(), $acf['id']);
					if(isset($fields) && count($fields)!=0){
						$loop_meta_acf = array();
						foreach ($fields as $field){
							$loop_meta_acf[$field['label']] = array(
									'label' => $field['label'],
									'name' => $field['name'],
									'type' => $field['type'],
									'key' => $field['key'],
									'id' => $field['id'],
									);
							if(isset($field['multiple'])){ $loop_meta_acf[$field['name']]['multiple'] = $field['multiple']; }
						}
					}
					if(isset($loop_meta_acf)){ $ultlbelements[$acf['title']] = $loop_meta_acf; }
				}
			}
		}
	}
	if(count($ultlbelements)!=0):
	foreach ($ultlbelements as $groups=>$fields){
		 
		foreach ($fields as $field){
			if($field['type'] == "google_map"){
				vc_map( array(
				"name" => $field['label'],
				"base" => 'wonderloops_acf_map_wflb_'.$field['name'].'_wflb',
				"icon" => "icon-wf-wp",
				"category" =>$groups,
				"weight" => -50,
				"params" => array(
				array(
				"type" => "hidden",
				"param_name" => "field",
				'value' => $field['name'],
				),
				array(
						"type" => "textfield",
						"heading" => __("Height in px", "wonder_loops"),
						"param_name" => "height",
				),
				wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
				array(
				"type" => "textfield",
				"heading" => __("Wrapper class name", "wonder_loops"),
				"param_name" => "wrapper_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wf_editor")
				),
				)
				));
			} elseif($field['type'] == "image") {
				vc_map( array(
				"name" => $field['label'],
				"base" => 'wonderloops_acf_image_wflb_'.$field['name'].'_wflb',
				"icon" => "icon-wf-wp",
				"category" =>$groups,
				"weight" => -50,
				"params" => array(
				array(
				"type" => "hidden",
				"param_name" => "field",
				'value' => $field['name'],
				),
				array(
				'type' => 'checkbox',
				'heading' => __( 'Show Captioned', 'wonder_loops' ),
				'param_name' => 'caption',
				'description' => __( 'If selected, Image will be shown in caption.', 'wonder_loops' ),
				'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
				),
				array(
				'type' => 'checkbox',
				'heading' => __( 'Zoom on Click', 'wonder_loops' ),
				'param_name' => 'zoom',
				'description' => __( 'If selected,on click prettyphoto will zoom', 'wonder_loops' ),
				'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
				),
				wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
				array(
				"type" => "textfield",
				"heading" => __("Wrapper class name", "wonder_loops"),
				"param_name" => "wrapper_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wf_editor")
				),
				)
				));
			} elseif($field['type'] == "file") {
				vc_map( array(
				"name" => $field['label'],
				"base" => 'wonderloops_acf_file_wflb_'.$field['name'].'_wflb',
				"icon" => "icon-wf-wp",
				"category" =>$groups,
				"weight" => -50,
				"params" => array(
				array(
				"type" => "hidden",
				"param_name" => "field",
				'value' => $field['name'],
				),
				array(
				'type' => 'checkbox',
				'heading' => __( 'Show To Members Only', 'wonder_loops' ),
				'param_name' => 'member_only',
				'description' => __( 'If selected, File will be members only', 'wonder_loops' ),
				'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
				),
				array(
				"type" => "textfield",
				"heading" => __("Download Button Text", "wonder_loops"),
				"param_name" => "text",
				),
				wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
				array(
				"type" => "textfield",
				"heading" => __("Wrapper class name", "wonder_loops"),
				"param_name" => "wrapper_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wf_editor")
				),
				)
				));
			} elseif($field['type'] == "select") {				
				vc_map( array(
				"name" => $field['label'],
				"base" => 'wonderloops_acf_select_wflb_'.$field['name'].'_wflb',
				"icon" => "icon-wf-wp",
				"category" =>$groups,
				"weight" => -50,
				"params" => array(
				array(
				"type" => "hidden",
				"param_name" => "field",
				'value' => $field['name'],
				),
				wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
				wfl_select_tags('items_wrapper',__('Wrap Items in','wonder_loops'),'span',array('none','p','div','span','ul','ol')),
				wfl_select_tags('itemtag',__('Item Tag','wonder_loops'),'span',array('p','div','span','li')),
					array(
							"type" => "textfield",
							"heading" => __("Pre Text", "wonder_loops"),
							"param_name" => "pre_text",
					),
					array(	"type" => "textfield",
						"heading" => __("Wrapper class name", "wonder_loops"),
						"param_name" => "wrapper_class",
					),
					array(
							"type" => "textfield",
							"heading" => __("Items Wrapper class name", "wonder_loops"),
							"param_name" => "item__wrapper_class",
					),
				)
				));
			} elseif($field['type'] == "checkbox") {
				vc_map( array(
				"name" => $field['label'],
				"base" => 'wonderloops_acf_checkbox_wflb_'.$field['name'].'_wflb',
				"icon" => "icon-wf-wp",
				"category" =>$groups,
				"weight" => -50,
				"params" => array(
				array(
				"type" => "hidden",
				"param_name" => "field",
				'value' => $field['name'],
				),
				wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
				wfl_select_tags('items_wrapper',__('Wrap Items in','wonder_loops'),'span',array('none','p','div','span','ul','ol')),
				wfl_select_tags('itemtag',__('Item Tag','wonder_loops'),'span',array('p','div','span','li')),
				array(
				"type" => "textfield",
				"heading" => __("Pre Text", "wonder_loops"),
				"param_name" => "pre_text",
				),
				array(
				"type" => "textfield",
				"heading" => __("Wrapper class name", "wonder_loops"),
				"param_name" => "wrapper_class",
				),
				array(
				"type" => "textfield",
				"heading" => __("Items Wrapper class name", "wonder_loops"),
				"param_name" => "item__wrapper_class",
				),
				)
				));
			} else {
				vc_map( array(
				"name" => $field['label'],
				"base" => 'wonderloops_acf_wflb_'.$field['name'].'_wflb',
				"icon" => "icon-wf-wp",
				"category" =>$groups,
				"weight" => -50,
				"params" => array(
				array(
				"type" => "hidden",
				"param_name" => "field",
				'value' => $field['name'],
				),
				array(
						"type" => "textfield",
						"heading" => __("Pre Text", "wonder_loops"),
						"param_name" => "pre_text",
				),
				wfl_select_tags('wrapper',__('Wrapper','wonder_loops'),'none',array('none','div','span')),
				array(
				"type" => "textfield",
				"heading" => __("Wrapper class name", "wonder_loops"),
				"param_name" => "wrapper_class",
				"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "wf_editor")
				),
				)
				));
			}
		}
	}
	endif;
	
}




/**
 * Whether the current request is in post type pages
 *
 * @param mixed $post_types
 * @return bool True if inside post type pages
 */
function wonderloops_is_post_type($post_types = ''){
	if(wonderloops_is_post_type_list($post_types) || wonderloops_is_post_type_new($post_types) || wonderloops_is_post_type_edit($post_types) || wonderloops_is_post_type_post($post_types) || wonderloops_is_post_type_taxonomy($post_types)){
		return true;
	}else{
		return false;
	}
}
/**
 * Whether the current request is in post type list page
 *
 * @param mixed $post_types
 * @return bool True if inside post type list page
 */
function wonderloops_is_post_type_list($post_types = '') {
	if ('edit.php' != basename($_SERVER['PHP_SELF'])) {
		return false;
	}
	if ($post_types == '') {
		return true;
	} else {
		$check = isset($_GET['post_type']) ? $_GET['post_type'] : (isset($_POST['post_type']) ? $_POST['post_type'] : 'post');
		if (is_string($post_types) && $check == $post_types) {
			return true;
		} elseif (is_array($post_types) && in_array($check, $post_types)) {
			return true;
		}
		return false;
	}
}

/**
 * Whether the current request is in post type new page
 *
 * @param mixed $post_types
 * @return bool True if inside post type new page
 */
function wonderloops_is_post_type_new($post_types = '') {
	if ('post-new.php' != basename($_SERVER['PHP_SELF'])) {
		return false;
	}
	if ($post_types == '') {
		return true;
	} else {
		$check = isset($_GET['post_type']) ? $_GET['post_type'] : (isset($_POST['post_type']) ? $_POST['post_type'] : 'post');
		if (is_string($post_types) && $check == $post_types) {
			return true;
		} elseif (is_array($post_types) && in_array($check, $post_types)) {
			return true;
		}
		return false;
	}
}
/**
 * Whether the current request is in post type post page
 *
 * @param mixed $post_types
 * @return bool True if inside post type post page
 */
function wonderloops_is_post_type_post($post_types = '') {
	if ('post.php' != basename($_SERVER['PHP_SELF'])) {
		return false;
	}
	if ($post_types == '') {
		return true;
	} else {
		$post = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post']) ? $_POST['post'] : false);
		$check = get_post_type($post);

		if (is_string($post_types) && $check == $post_types) {
			return true;
		} elseif (is_array($post_types) && in_array($check, $post_types)) {
			return true;
		}
		return false;
	}
}
/**
 * Whether the current request is in post type edit page
 *
 * @param mixed $post_types
 * @return bool True if inside post type edit page
 */
function wonderloops_is_post_type_edit($post_types = '') {
	if ('post.php' != basename($_SERVER['PHP_SELF'])) {
		return false;
	}
	$action = isset($_GET['action']) ? $_GET['action'] : (isset($_POST['action']) ? $_POST['action'] : '');
	if ('edit' != $action) {
		return false;
	}

	if ($post_types == '') {
		return true;
	} else {
		$post = isset($_GET['post']) ? $_GET['post'] : (isset($_POST['post']) ? $_POST['post'] : false);
		$check = get_post_type($post);

		if (is_string($post_types) && $check == $post_types) {
			return true;
		} elseif (is_array($post_types) && in_array($check, $post_types)) {
			return true;
		}
		return false;
	}
}
/**
 * Whether the current request is in post type taxonomy pages
 *
 * @param mixed $post_types
 * @return bool True if inside post type taxonomy pages
 */
function wonderloops_is_post_type_taxonomy($post_types = '') {
	if ('edit-tags.php' != basename($_SERVER['PHP_SELF'])) {
		return false;
	}
	if ($post_types == '') {
		return true;
	} else {
		$check = isset($_GET['post_type']) ? $_GET['post_type'] : (isset($_POST['post_type']) ? $_POST['post_type'] : 'post');
		if (is_string($post_types) && $check == $post_types) {
			return true;
		} elseif (is_array($post_types) && in_array($check, $post_types)) {
			return true;
		}
		return false;
	}
}
function wfl_ReadMore(){
	return array(
			'type' => 'checkbox',
			'heading' => __( 'Show Read More?', 'wonder_loops' ),
			'param_name' => 'read_more',
			'description' => __( 'If selected, Read More will be shown.', 'wonder_loops' ),
			'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
	);
}

function wfl_link_to_post(){
	return array(
			'type' => 'checkbox',
			'heading' => __( 'Link to post?', 'wonder_loops' ),
			'param_name' => 'link_to_post',
			'description' => __( 'If selected, Item will link to post.', 'wonder_loops' ),
			'value' => array( __( 'Yes, please', 'wonder_loops' ) => 'yes' )
	);
}
function wfl_select_tags ($name,$title,$default,$values){
	$tags = array(
        'type' => 'dropdown',
        'heading' => $title,
        'param_name' => $name,
        'value' =>$values,
        'std' => $default,
      );
	return $tags;
}

function wfl_tag_open($wrapper,$wrapperclass=null){
	$wrapopen='';
	if($wrapper!='none'){
		$wrapopen = '<'.$wrapper.' class="'.$wrapperclass.'">'."\n";
	}
	return $wrapopen;
}
function wfl_tag_close($wrapper){
	$wrapclose='';
	if($wrapper!='none'){
		$wrapclose = '</'.$wrapper.'>'."\n";
	}
	return $wrapclose;
}

function wfl_pre_text($pre_text,$pre_text_tag){
	$pretext='';
	if(strlen($pre_text)!=0)
	if($pre_text_tag!='none'){
		$pretext .= '<'.$pre_text_tag.' class="'.$wrapperclass.'">'."\n";
	}
	$pretext .= $pre_text;
	if($pre_text_tag!='none'){
		$pretext .= '</'.$pre_text_tag.'>'."\n";
	}
	
	return $pretext;
}