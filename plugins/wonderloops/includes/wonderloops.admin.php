<?php
class WonderLoopsAdmin {
	
	function __construct(){
		add_action('admin_menu', array($this,'admin_menu'));
		add_action( 'admin_enqueue_scripts', array($this,'load_custom_admin_styles') );
		add_action( 'save_post', array($this,'meta_save_loop_items'),1,2 );
		add_action( 'delete_post', array($this,'delete_loop') );
		
		foreach(glob(WONDERLOOPS_SHORTCODES."/*.php") as $shortcode)
		{
			require_once($shortcode);
		}
		if(wonderloops_is_post_type('wonderloops') || get_post_type( $_POST['post_id'] ) == 'wonderloops'){
			 vc_disable_frontend();
			 add_action( 'admin_init', 'get_acf_fields',1 );
			 //vc_remove_element("vc_column_text");
			 vc_remove_element("vc_message");
			 vc_remove_element("vc_single_image");
			 vc_remove_element("vc_toggle");
			 vc_remove_element("vc_gallery");
			 vc_remove_element("vc_images_carousel");
			 vc_remove_element("vc_teaser_grid");
			 vc_remove_element("vc_posts_grid");
			 vc_remove_element("vc_carousel");
			 vc_remove_element("vc_posts_slider");
			 vc_remove_element("vc_widget_sidebar");
			 vc_remove_element("vc_button");
			 vc_remove_element("vc_button2");
			 vc_remove_element("vc_cta_button");
			 vc_remove_element("vc_cta_button2");
			 vc_remove_element("vc_video");
			 vc_remove_element("vc_gmaps");
			 vc_remove_element("vc_flickr");
			 vc_remove_element("vc_progress_bar");
			 vc_remove_element("vc_pie");
			 vc_remove_element("layerslider_vc");
			 vc_remove_element("rev_slider_vc");
			 vc_remove_element("vc_wp_search");
			 vc_remove_element("vc_wp_meta");
			 vc_remove_element("vc_wp_recentcomments");
			 vc_remove_element("vc_wp_calendar");
			 vc_remove_element("vc_wp_pages");
			 vc_remove_element("vc_wp_tagcloud");
			 vc_remove_element("vc_wp_custommenu");
			 vc_remove_element("vc_wp_text");
			 vc_remove_element("vc_wp_posts");
			 vc_remove_element("vc_wp_links");
			 vc_remove_element("vc_wp_categories");
			 vc_remove_element("vc_wp_archives");
			 vc_remove_element("vc_wp_rss");
		}
		if(wonderloops_is_post_type('wonderloops')){
			add_action( 'admin_enqueue_scripts', array($this,'load_wonderloops_admin_styles') );
			add_action( 'admin_enqueue_scripts', array($this,'load_wonderloops_admin_scripts') );
			add_action( 'admin_menu', array($this,'metabox_postTypeSelector'), 1 );
		}
	}
	function delete_loop($pid){
		$file = WONDERLOOPS_CACHE_DIR.'/'.$pid.'.php';
		if(file_exists($file)){
			unlink($file);
		}
	}
	public function admin_menu() {
		add_submenu_page('edit.php?post_type=wonderloops', __("Custom Fields",'acf'), __("Custom Fields",'acf'), 'manage_options', 'edit.php?post_type=acf', false);
	
	}
	
	function load_custom_admin_styles() {
		wp_register_style( 'wonderloops', WONDERLOOPS_CSS .'wonderloops.css', false, '1.0.0' );
		wp_enqueue_style( 'wonderloops' );
	}
	
	function load_wonderloops_admin_styles(){
		wp_enqueue_style( 'wonderloops-markitup-style', WONDERLOOPS_CSS .'style.css', false, '1.0.0' );
		wp_enqueue_style( 'wonderloops-markitup-set', WONDERLOOPS_CSS .'set.css', false, '1.0.0' );
	}
	
	function load_wonderloops_admin_scripts(){
		wp_enqueue_script( 'code-lover-markitup-local', WONDERLOOPS_JS.'codeLover.js', array( 'jquery', 'jquery-ui-core' ) );
		wp_enqueue_script( 'code-lover-markitup-js', WONDERLOOPS_JS.'jquery.markitup.js', array( 'jquery', 'jquery-ui-core' ) );
		wp_enqueue_script( 'code-lover-markitup-set', WONDERLOOPS_JS.'set.js');
	}
	
	function showloopposttype(){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-ptype',true);
		echo $cura;
	}
	
	
	function metabox_postTypeSelector(){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-ptype',true);
		if(strlen($cura)==0){
			add_meta_box('ultimatumlb_pselector',__( 'Select Post Type', 'wonderloops'), array( $this, 'selectposttypeform'),'wonderloops','normal','high');
		} else {
			add_meta_box('ultimatumlb_pdisplay',__( 'Loop Post Type', 'wonderloops'), array( $this, 'showloopposttype'),'wonderloops','side','high');
			// Multi Loop Column count
			//structural adds
			// Post Defaults
			// Pre Loop & Post
			add_meta_box('wonderloops_preloop',__( 'Before Loop', 'wonderloops'), array( $this, 'before_loop'),'wonderloops','normal','high');
			add_meta_box('wonderloops_prepost',__( 'Before post', 'wonderloops'), array( $this, 'before_post'),'wonderloops','normal','high');
			// After post & Loop
			add_meta_box('wonderloops_afpost',__( 'After Post', 'wonderloops'), array( $this, 'after_post'),'wonderloops','advanced','high');
			add_meta_box('wonderloops_afloop',__( 'After Loop', 'wonderloops'), array( $this, 'after_loop'),'wonderloops','advanced','high');
			add_meta_box('wonderloops_colcount',__( 'Column Count', 'wonderloops'), array( $this, 'col_count'),'wonderloops','side','high');
			
		}
	}
	
	function selectposttypeform(){
		$args=array('public'   => true,'publicly_queryable' => true);
		$post_types=get_post_types($args,'names');
		echo '<p>'.__('To continue please select a post type for your loop and save it once','wonderloops').'</p>';
		echo '<select name="wonderloops-ptype">';
		foreach ($post_types as $post_type ) {
			if($post_type!='attachment' && $post_type!='forum' && $post_type!='topic' && $post_type!='reply' && $post_type!='wondocs' && $post_type!='form' && $post_type!='essential_grid'){
				echo '<option value="'.$post_type.'">'.$post_type.'</option>';
			}
		}
		echo '<option value="page">page</option>';
		echo '</select>';
		?>
		<script>
		jQuery( document ).ready(function() {
			jQuery('#wf_wfeditor').remove();
		});
			
		</script>
		<?php 
	}
	
	function before_loop(){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-befloop',true);
		echo'<p>';
		_e('You can type any HTML/PHP that you want to be parsed before the Loop','wonderloops');
		echo'</p>';
		echo'<textarea class="codeLovertextarea" name="wonderloops-befloop">'.$cura.'</textarea><input type="hidden" name="wpb_js_composer_group_access_show_rule" class="wpb_js_composer_group_access_show_rule" value="only" />';
	
	}
	
	function before_post(){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-befpost',true);
		echo'<p>';
		_e('You can type any HTML/PHP that you want to be parsed before the Post itself. This will be repeated for each post on page.','wonderloops');
		echo'</p>';
		echo'<textarea class="codeLovertextarea" name="wonderloops-befpost">'.$cura.'</textarea>';
	}
	
	function after_post(){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-afpost',true);
		echo'<p>';
		_e('You can type any HTML/PHP that you want to be parsed after the Post itself. This will be repeated for each post on page.','wonderloops');
		echo'</p>';
		echo'<textarea class="codeLovertextarea" name="wonderloops-afpost">'.$cura.'</textarea>';
	}
	
	function after_loop(){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-afloop',true);
		echo'<p>';
		_e('You can type any HTML/PHP that you want to be parsed after the Loop','wonderloops');
		echo'</p>';
		echo'<textarea class="codeLovertextarea" name="wonderloops-afloop">'.$cura.'</textarea>';
	}
	
	function col_count(){
		$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
		$cura= get_post_meta($post_id,'wonderloops-colcount',true);
		echo'<p>';
		_e('How many columns will be shown in multi post?','wonderloops');
		echo'</p>';
		echo '<select name="wonderloops-colcount">';
		for($i=1;$i<=4;$i++){
			echo '<option value="'.$i.'" '.selected($cura,$i).'>'.$i.' '.__('columns','wonderloops').'</option>';
		}
		echo '</select>';
	
	}
	
	function meta_save_loop_items( $post_id, $post ) {
		//* Don't try to save the data under autosave, ajax, or future post.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return;
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
			return;
		if ( defined( 'DOING_CRON' ) && DOING_CRON )
			return;
		//* Grab the post object
		$post = get_post( $post );
	
		//* Don't save if WP is creating a revision (same as DOING_AUTOSAVE?)
		if ( 'revision' === $post->post_type )
			return;
		//* Check that the user is allowed to edit the post
		if ( ! current_user_can( 'edit_post', $post->ID ) )
			return;
		$save = false;
		foreach($_POST as $p=>$value){
			if(preg_match('/wonderloops-/i',$p)){
				update_post_meta($post->ID, $p, $value);
				if($p!='wonderloops-ptype') $save = true;
			}
		}
		if($save){
			include(WONDERLOOPS_INCLUDES.'/generator.php');
			wonderloops_print_loop($post->ID);
			
		}
	}
	
}