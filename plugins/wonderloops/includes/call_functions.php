<?php
function wflb_ggogle_maps( $atts ) {
	wp_enqueue_script('jquery-google-maps');
	extract(shortcode_atts(array(
	"width" => '100%',
	"height" => '400',
	"address" => '',
	"latitude" => 0,
	"longitude" => 0,
	"zoom" => 16,
	"html" => '',
	"popup" => 'false',
	"controls" => 'true',
	'pancontrol' => 'true',
	'zoomcontrol' => 'true',
	'maptypecontrol' => 'true',
	'scalecontrol' => 'true',
	'streetviewcontrol' => 'true',
	'overviewmapcontrol' => 'true',
	"scrollwheel" => 'true',
	'doubleclickzoom' =>'true',
	"maptype" => 'ROADMAP',
	"marker" => 'true',
	'align' => false,
	), $atts));
	if($width){
		if(is_numeric($width)){
			$width = $width.'px';
		}
		$width = 'width:'.$width.';';
	}else{
		$width = '';
		$align = false;
	}
	if($height){
		if(is_numeric($height)){
			$height = $height.'px';
		}
		$height = 'height:'.$height.';';
	}else{
		$height = '';
	}
	$html = str_replace('{linebreak}', '<br/>', $html);

	/* fix */
	$search  = array('G_NORMAL_MAP', 'G_SATELLITE_MAP', 'G_HYBRID_MAP', 'G_DEFAULT_MAP_TYPES', 'G_PHYSICAL_MAP');
	$replace = array('ROADMAP', 'SATELLITE', 'HYBRID', 'HYBRID', 'TERRAIN');
	$maptype = str_replace($search, $replace, $maptype);
	/* end fix */

	if($controls == 'true'){
		$controls = <<<HTML
{
	panControl: {$pancontrol},
	zoomControl: {$zoomcontrol},
	mapTypeControl: {$maptypecontrol},
	scaleControl: {$scalecontrol},
	streetViewControl: {$streetviewcontrol},
	overviewMapControl: {$overviewmapcontrol}
}
HTML;
	}

	$align = $align?' align'.$align:'';
	$id = rand(100,1000);
	if($marker != 'false'){
		return <<<HTML
<div id="google_map_{$id}" class="google_map{$align}" style="{$width}{$height}"></div>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var tabs = jQuery("#google_map_{$id}").parents('.tabs_container,.mini_tabs_container,.accordion');
	jQuery("#google_map_{$id}").bind('initGmap',function(){
		jQuery(this).gMap({
			zoom: {$zoom},
			markers:[{
				address: "{$address}",
				latitude: {$latitude},
				longitude: {$longitude},
				html: "{$html}",
				popup: {$popup}
			}],
			controls: {$controls},
			maptype: '{$maptype}',
			doubleclickzoom:{$doubleclickzoom},
			scrollwheel:{$scrollwheel}
		});
		jQuery(this).data("gMapInited",true);
	}).data("gMapInited",false);
	if(tabs.size()!=0){
		tabs.find('ul.tabs,ul.mini_tabs,.accordion').data("tabs").onClick(function(index) {
			this.getCurrentPane().find('.google_map').each(function(){
				if(jQuery(this).data("gMapInited")==false){
					jQuery(this).trigger('initGmap');
				}
			});
		});
	}else{
		jQuery("#google_map_{$id}").trigger('initGmap');
	}
	jQuery('.wpb_tour_tabs_wrapper').tabs({
        show: function(e, ui) {
            jQuery("#google_map_{$id}").trigger('initGmap');
        }
    });
});
</script>
HTML;
	}else{
		return <<<HTML
<div id="google_map_{$id}" class="google_map{$align}" style="{$width}{$height}"></div>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var tabs = jQuery("#google_map_{$id}").parents('.tabs_container,.mini_tabs_container,.accordion');
	jQuery("#google_map_{$id}").bind('initGmap',function(){
		jQuery("#google_map_{$id}").gMap({
			zoom: {$zoom},
			latitude: {$latitude},
			longitude: {$longitude},
			address: "{$address}",
			controls: {$controls},
			maptype: '{$maptype}',
			doubleclickzoom:{$doubleclickzoom},
			scrollwheel:{$scrollwheel}
		});
		jQuery(this).data("gMapInited",true);
	}).data("gMapInited",false);
	if(tabs.size()!=0){
		tabs.find('ul.tabs,ul.mini_tabs,.accordion').data("tabs").onClick(function(index) {
			this.getCurrentPane().find('.google_map').each(function(){
				if(jQuery(this).data("gMapInited")==false){
					jQuery(this).trigger('initGmap');
				}
			});
		});
	}else{
		jQuery("#google_map_{$id}").trigger('initGmap');
	}
});
</script>
HTML;
	}
}


function wflb_post_gallery($instance,$zoom=false){
	global $post;
	$link = false;
	if(!is_singular()){
		$link = get_permalink();
	}
	wp_enqueue_script('slider-flex');
	$image_ids_str = get_post_meta($post->ID,'_image_ids',true);
	$image_ids = explode(',',str_replace('image-','',$image_ids_str));
	if(count($image_ids)!=0){
		
	foreach($image_ids as $image_id){
		$images[] = array(
				'image_id'=>$image_id,
				'title' => get_post_field('post_title', $image_id),
				'link' => $link,
				'target' => '_self',
				'video' =>get_post_meta(get_the_ID(),'ultimatum_video',true),
		);
	}
	?>
	<div class="flex-container">
	<div class="flexslider" id="<?php echo $post->ID.'-post-flex'; ?>" >
	<ul class="slides">
	
	<?php
	foreach($images as $image){
	$src = wp_get_attachment_image_src( $image['image_id'],"full");
	$imgsrc = $src[0]
	?>
	<li>
	<?php if(isset($image["link"]) && strlen($image['link'])!=0){?>
	<a href="<?php echo $image["link"]; ?>"><img src="<?php echo $imgsrc; ?>" style="float:right" alt="<?php echo $image["title"]; ?>" title="<?php echo $image["title"]; ?>" /></a>
	<?php } else { ?>
	<?php if($zoom=='yes') { ?>
	<a href="<?php echo $imgsrc; ?>" class="prettyphoto">
	<?php  } ?>
	<img src="<?php echo $imgsrc; ?>" style="float:right" alt="<?php echo $image["title"]; ?>" title="<?php echo $image["title"]; ?>" />
	<?php if($zoom=='yes') { ?>
	</a>
	<?php  } ?>
	<?php  } ?>
	</li>
	<?php }	?>
	</ul>
	</div>
	</div>
	<script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function() {
	jQuery('#<?php echo $post->ID.'-post-flex'; ?>').flexslider({
		 animation: "slide",
		 maxItems:1
	
	});
	});
	//]]>
</script>
	<?php
	} 
	}