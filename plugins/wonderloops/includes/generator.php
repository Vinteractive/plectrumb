<?php
// This will write the php files!!
function wonderloops_print_loop($postid){
	require_once('trickster_map.php');
	$content  = "<?php"."\n";
	$content .= "/*"."\n";
	$content .= " * Loop Generator: WonderLoops"."\n";
	$content .= " * Loop Name: ".get_the_title($postid)."\n";
	$content .= " * Loop File: ".$postid.'.php'."\n";
	$content .= " */"."\n";
	$content .= "wp_enqueue_style( 'js_composer_front' );"."\n";
	$content .= "wp_enqueue_script( 'wpb_composer_front_js' );"."\n";
	$content .= "wp_enqueue_style('js_composer_custom_css');"."\n";
	$content .= "wp_enqueue_script('jquery-ui-accordion');"."\n";
	$content .= "wp_enqueue_script( 'jquery-ui-tabs' );"."\n";
	$content .= "wp_enqueue_script('jquery_ui_tabs_rotate');"."\n";
	$content .= "require_once (WONDERLOOPS_INCLUDES.'/call_functions.php');"."\n";
	$columns = get_post_meta($postid,'wonderloops-colcount',true);
	$content .= "?>"."\n";
	$post_custom_css = get_post_meta($postid, '_wpb_post_custom_css', true);
	if (!empty($post_custom_css)) {
		$content .= '<style type="text/css">'."\n";;
		$content .= $post_custom_css."\n";;
		$content .= '</style>'."\n";
	}
	$shortcodes_custom_css = get_post_meta($postid, '_wpb_shortcodes_custom_css', true);
	if (!empty($shortcodes_custom_css)) {
		$content .= '<style type="text/css">'."\n";
		$content .= str_replace('!important', '',$shortcodes_custom_css)."\n";
		$content .= '</style>'."\n";
	}
	// get before loop
	$content .=	get_post_meta($postid,'wonderloops-befloop',true)."\n";
	// Start Loop
	$content .= "<?php"."\n";
	$content .= 'if(!is_singular() || $a_custom_loop){'."\n";
	if($columns!=1){
		$content .='$colcount ='.$columns.';'."\n";
		if($columns ==2){
			$content .='add_filter( "post_class", "ultimatum_entry_post_class_half" );'."\n";
		}
		if($columns ==3){
			$content .='add_filter( "post_class", "ultimatum_entry_post_class_third" );'."\n";
		}
		if($columns ==4){
			$content .='add_filter( "post_class", "ultimatum_entry_post_class_fourth" );'."\n";
		}
	}
	$content .= "}"."\n";
	$content .= '$igogrid=1;'."\n";
	$content .= 'if($a_custom_loop){'."\n";
	$content .= 'if ($r->have_posts() ) {'."\n";
	$content .= 'while ( $r->have_posts() ) { '."\n";
	$content .= '$r->the_post();'."\n";
	$content .= 'global $post;'."\n";
	$content .= '$clear=true;'."\n";
	$content .= 'if($colcount!=1)://gridd'."\n";
	$content .= 'if($igogrid==1){'."\n";
		$content .= '$igogrid++;'."\n";
		$content .= 'remove_filter( "post_class", "ultimatum_entry_post_class_last" );'."\n";
		$content .= '$clear=false;'."\n";
	$content .= '} elseif($igogrid==$colcount){'."\n";
		$content .= 'add_filter( "post_class", "ultimatum_entry_post_class_last" );'."\n";
		$content .= '$clear=true;'."\n";
		$content .= '$igogrid=1;'."\n";
	$content .= '} else{'."\n";
		$content .= '$igogrid++;'."\n";
		$content .= 'remove_filter( "post_class", "ultimatum_entry_post_class_last" );'."\n";
		$content .= '$clear=false;'."\n";
	$content .= '}'."\n";
	$content .='endif;//gridd'."\n";
	$content .= "?>"."\n";
	
	$content .='<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>'."\n";
	// get before post
	$content .=	get_post_meta($postid,'wonderloops-befpost',true)."\n";
	$content_post = get_post($postid);
	$contentp = preg_replace("@_wflb.*?wflb@","",$content_post->post_content);
	$content .= do_shortcode($contentp);
	
	// get after post
	
	$content .=	get_post_meta($postid,'wonderloops-afpost',true)."\n";
	$content .='</article>'."\n";
	$content .= "<?php"."\n";
	$content .= 'if($clear) echo \'<div class="clearfix"></div>\';'."\n";
	$content .= "} // end while"."\n";
	$content .= '} else {'."\n";
	$content .= 'do_action( "ultimatum_loop_else" );'."\n";
	$content .= "} // end if"."\n";
	$content .= "?>"."\n";
	
	$content .= "<?php"."\n";
	$content .= '} else {'."\n";
	$content .= "if ( have_posts() ) {"."\n";
	$content .= "while ( have_posts() ) { "."\n";
	$content .= "the_post();"."\n";
	$content .= 'global $post;'."\n";
	$content .= '$clear=true;'."\n";
	$content .= 'if($colcount!=1 && !is_singular() )://gridd'."\n";
	$content .= 'if($igogrid==1){'."\n";
	$content .= '$igogrid++;'."\n";
	$content .= 'remove_filter( "post_class", "ultimatum_entry_post_class_last" );'."\n";
	$content .= '$clear=false;'."\n";
	$content .= '} elseif($igogrid==$colcount){'."\n";
	$content .= 'add_filter( "post_class", "ultimatum_entry_post_class_last" );'."\n";
	$content .= '$clear=true;'."\n";
	$content .= '$igogrid=1;'."\n";
	$content .= '} else{'."\n";
	$content .= '$igogrid++;'."\n";
	$content .= 'remove_filter( "post_class", "ultimatum_entry_post_class_last" );'."\n";
	$content .= '$clear=false;'."\n";
	$content .= '}'."\n";
	$content .='endif;//gridd'."\n";
	$content .= "?>"."\n";
	
	$content .='<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>'."\n";
	// get before post
	$content .=	get_post_meta($postid,'wonderloops-befpost',true)."\n";
	$content_post = get_post($postid);
	$contentp = preg_replace("@_wflb.*?wflb@","",$content_post->post_content);
	$content .= do_shortcode($contentp);
	
	// get after post
	
	$content .=	get_post_meta($postid,'wonderloops-afpost',true)."\n";
	$content .='</article>'."\n";
	$content .= "<?php"."\n";
	$content .= 'if($clear) echo \'<div class="clearfix"></div>\';'."\n";
	$content .= "} // end while"."\n";
	$content .= 'do_action( "ultimatum_after_endwhile",$instance );'."\n";
	$content .= ' } else {'."\n";
	$content .= 'do_action( "ultimatum_loop_else" );'."\n";
	$content .= "} // end if"."\n";
	$content .= "?>"."\n";
	
	$content .= "<?php"."\n";
	$content .= '}'."\n";
	$content .= "?>"."\n";
	
	
	
	
	// get after loop
	$content .=	get_post_meta($postid,'wonderloops-afloop',true)."\n";
	/*
	global $shortcode_tags;
	foreach($shortcode_tags as $code => $function)
	{
		$content .= "<li>".$code.'</li>';
	}
	*/ 
	$file = WONDERLOOPS_CACHE_DIR.'/'.$postid.'.php';
	if(file_exists($file)){unlink($file);}
	$fhandle = @fopen($file, 'w+');
	if ($fhandle) fwrite($fhandle, stripslashes($content), strlen(stripslashes($content)));
}
